<%-- 
    Document   : index
    Created on : 29-03-2020, 13:07:46
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Kevin</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    
    <!-- FRAMEWORK BOOTSTRAP-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    
    </head>
    <body>
         <h1>Actividad Sumativa - EJE01</h1>
         
         <form name="form" action="controlador" method="POST">
         
         <div style="width: 20%; padding: 20px">
         <div class="form-group">
              <input type="text" class="form-control" placeholder="Nombre" name="nombre"/>
         </div>
         
           <div class="form-group">
              <input type="text" class="form-control" placeholder="Sección" name="seccion"/>
         </div>
         </div>
         
         <div style="padding: 20px; position: relative; bottom: 42px">
         <button type="submit" class="btn btn-primary">Registrar</button>
         </div>
             
         </form>
    </body>
</html>
